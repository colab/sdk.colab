var http;
(function (http) {
    "use strict";
    //State
    var headers = {};
    //Functions
    function requestJSON(url, method, data) {
        return new Promise((resolve, reject) => {
            var x = new XMLHttpRequest();
            x.open(method, url);
            for (var i in headers) {
                x.setRequestHeader(i, headers[i]);
            }
            x.onerror = (error) => {
                reject(error);
            };
            x.onload = (result) => {
                if (result.target['status'] !== 200) {
                    reject('request failed w/ code ' + result.target['status']);
                }
                resolve(JSON.parse(result.target['response']));
            };
            if (data && typeof data === 'object') {
                x.send(JSON.stringify(data));
            }
            else {
                x.send();
            }
        });
    }
    http.requestJSON = requestJSON;
    function getJSON(url) {
        return requestJSON(url, 'GET');
    }
    http.getJSON = getJSON;
    function postJSON(url, data) {
        return requestJSON(url, 'POST', data);
    }
    http.postJSON = postJSON;
    function setHeaders(new_headers) {
        for (var i in new_headers) {
            headers[i] = new_headers[i];
        }
    }
    http.setHeaders = setHeaders;
})(http || (http = {}));
var colab;
(function (colab) {
    "use strict";
    //Event stuff
    var callbacks = {};
    function on(event, callback) {
        if (!(event in callbacks)) {
            callbacks[event] = [];
        }
        callbacks[event].push(callback);
    }
    function off(event, callback) {
        if (event in callbacks) {
            delete callbacks[event][callbacks[event].indexOf(callback)];
        }
    }
    function dispatch(event, data) {
        for (var i in callbacks[event]) {
            callbacks[event][i](data);
        }
    }
    //STATE!
    console.log('sdk.colab running...');
    colab.me = null;
    colab.settings = {
        clientId: '',
        scopes: ['basic', 'identity:netid:read'],
        state: Math.round(1 + Math.random() * 4294967296),
        token: '',
        tokenExpiration: null,
        loginButtonSelector: '.netid-login',
        oauthHost: 'oauth.oit.duke.edu',
        oauthAuthorizationPath: '/oauth/authorize.php',
        loginButtonSource: 'iframe.html'
    };
    var loginButtonEls = null;
    //Imperatives
    document.addEventListener('DOMContentLoaded', function () {
        setupLoginButtons();
    });
    window.addEventListener('message', function (event) {
        if (event.origin === "https://cdn.colab.duke.edu") {
        }
        if (event.data === "netidLoginRequested") {
            login();
        }
    });
    //public functions
    function init(options) {
        if (typeof options === 'object') {
            for (var i in options) {
                colab.settings[i] = options[i];
            }
        }
        setInterval(checkIfTokenExpired, 30000);
        http.setHeaders({ 'x-api-key': colab.settings.clientId });
    }
    colab.init = init;
    function login() {
        //set state to random number
        colab.settings.state = Math.round(1 + Math.random() * 4294967296);
        if (localStorage['scopes']) {
            try {
                var oldScopes = JSON.parse(localStorage['scopes']);
                var canReuseToken = true;
                for (var i = 0; i < colab.settings.scopes.length; i++) {
                    canReuseToken = canReuseToken && oldScopes.indexOf(colab.settings.scopes[i]) < 0;
                }
                if (canReuseToken) {
                    setToken({ state: "" + colab.settings.state, access_token: localStorage['token'], expires_in: localStorage['tokenExpiration'] });
                    return updateMe();
                }
            }
            catch (e) {
                console.log("Scopes found in cache, but couldn't parse them");
            }
        }
        var qs = "?response_type=token"
            + "&scope=" + encodeURIComponent(colab.settings.scopes.join(' '))
            + "&client_id=" + encodeURIComponent(colab.settings.clientId)
            + "&state=" + encodeURIComponent("" + colab.settings.state)
            + "&redirect_uri=" + encodeURIComponent(qualifyURL('oauth2callback.html'));
        window.open('https://' + colab.settings.oauthHost + colab.settings.oauthAuthorizationPath + qs, '_blank');
        return new Promise((resolve, reject) => {
            on('tokenset', (token) => {
                resolve({});
            });
            on('tokenerror', (err) => {
                reject(err);
            });
        })
            .then(updateMe);
    }
    colab.login = login;
    function setToken(tokenResult) {
        if (parseInt(tokenResult.state, 10) !== colab.settings.state) {
            //ignore instead
            console.log('State mismatch: ' + colab.settings.state + ' (correct) vs. ' + parseInt(tokenResult.state, 10) + '. Ignoring...');
            return;
        }
        if (tokenResult.error) {
            dispatch('tokenerror', tokenResult.error + ': ' + tokenResult.error_description);
        }
        colab.settings.token = tokenResult.access_token;
        http.setHeaders({ 'Authorization': 'Bearer ' + colab.settings.token });
        colab.settings.tokenExpiration = Date.now() + 1000 * parseInt(tokenResult.expires_in, 10);
        localStorage['token'] = colab.settings.token;
        localStorage['tokenExpiration'] = colab.settings.tokenExpiration;
        localStorage['scopes'] = JSON.stringify(decodeURIComponent(tokenResult.scope).split(/[\s\+]/));
        dispatch('tokenset', colab.settings.token);
    }
    colab.setToken = setToken;
    function updateMe() {
        return http.getJSON('https://api.colab.duke.edu/identity/v1')
            .then(function (m) {
            colab.me = m;
            for (var i = 0; i < loginButtonEls.length; i++) {
                //var loggedInEl = document.createElement('span');
                console.log("Logged in as " + colab.me['displayName'] + " (" + colab.me['netid'] + ")");
            }
            return colab.me;
        });
    }
    colab.updateMe = updateMe;
    function getIdentity(netid) {
        return http.getJSON('https://api.colab.duke.edu/identity/' + netid);
    }
    colab.getIdentity = getIdentity;
    //private functions
    function setupLoginButtons() {
        loginButtonEls = loginButtonEls || document.querySelectorAll(colab.settings.loginButtonSelector);
        for (var i = 0; i < loginButtonEls.length; i++) {
            var loginButton = document.createElement('iframe');
            loginButton.className = 'netid-login';
            loginButton.src = colab.settings.loginButtonSource;
            loginButton.width = '250px';
            loginButton.height = '49px';
            loginButton.frameBorder = '0';
            loginButtonEls[i].parentElement.replaceChild(loginButton, loginButtonEls[i]);
        }
        loginButtonEls = document.querySelectorAll('.netid-login');
        on('tokenset', function () {
            for (var i = 0; i < loginButtonEls.length; i++) {
                loginButtonEls[i].remove();
            }
        });
    }
    function qualifyURL(url) {
        function escapeHTML(s) {
            return s.split('&').join('&amp;').split('<').join('&lt;').split('"').join('&quot;');
        }
        var el = document.createElement('div');
        el.innerHTML = '<a href="' + escapeHTML(url) + '">x</a>';
        return el.firstChild.href;
    }
    function checkIfTokenExpired() {
        if (Date.now() + 30000 > colab.settings.tokenExpiration) {
            onTokenExpired();
        }
    }
    function onTokenExpired() {
        var loginAgain = confirm('Your netid session is about to expire, want to log in again?');
        if (colab.settings.token && loginAgain) {
            login();
        }
        else {
            setupLoginButtons();
            colab.settings.token = null;
        }
    }
    ;
})(colab || (colab = {}));
