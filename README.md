# The Innovation Co-Lab SDK

The Co-Lab SDK allows applications to interface with Duke's APIs as well as log users in using netids. This repository contains the Co-Lab SDKs that have been built for various stacks, organized by language. 

## Stacks currently supported

 - Front-end JS
 - Front-end TS
 
## How to use

### JS
    
Use a `script` tag to reference `sdk.colab.js` from any pages that may want to use its functionality. Place `oauth2callback.html `in the same directory as any pages that may be using netid login, copying as necessary maki sure that each one of them is added as a `redirectURI` in your app registration. Then, write an init line like the one below on your page!
    
```javascript
    colab.init({clientId: 'myClientId'});
```
    
#### JS SDK Reference
    
##### colab.getIdentity(netid)    
    
### TS

    