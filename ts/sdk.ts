module http {
    "use strict";
    //State
    var headers = {};

    //Functions

    export function requestJSON(url: string, method: string, data?: any): Promise<string> {
        return new Promise((resolve, reject) => {
            var x = new XMLHttpRequest();
            x.open(method, url);
            for (var i in headers) {
                x.setRequestHeader(i, headers[i]);
            }
            x.onerror = (error: ProgressEvent) => {
                reject(error);
            }
            x.onload = (result: ProgressEvent) => {
                if (result.target['status'] !== 200) { reject('request failed w/ code ' + result.target['status']); }
                resolve(JSON.parse(result.target['response']));
            }
            if (data && typeof data === 'object') {
                x.send(JSON.stringify(data));
            } else {
                x.send();
            }

        });
    }

    export function getJSON(url: string): Promise<any> {
        return requestJSON(url, 'GET');
    }

    export function postJSON(url: string, data: any): Promise<any> {
        return requestJSON(url, 'POST', data);
    }

    export function setHeaders(new_headers: any): void {
        for (var i in new_headers) {
            headers[i] = new_headers[i];
        }
    }
}


module colab {
    "use strict";
    //Event stuff
    var callbacks: { [id: string]: ((event: any) => void)[] } = {};
    function on(event: string, callback: (event: any) => void): void {
        if (!(event in callbacks)) { callbacks[event] = []; }
        callbacks[event].push(callback);
    }

    function off(event: string, callback: (event: any) => void): void {
        if (event in callbacks) {
            delete callbacks[event][callbacks[event].indexOf(callback)];
        }
    }

    function dispatch(event: string, data: any) {
        for (var i in callbacks[event]) {
            callbacks[event][i](data);
        }
    }

    //STATE!
    console.log('sdk.colab running...');
    export var me: Identity = null;
    export var settings: initOptions = {
        clientId: '',
        scopes: ['basic', 'identity:netid:read'],
        state: Math.round(1 + Math.random() * 4294967296),
        token: '',
        tokenExpiration: null,
        loginButtonSelector: '.netid-login',
        oauthHost: 'oauth.oit.duke.edu',
        oauthAuthorizationPath: '/oauth/authorize.php',
        loginButtonSource: 'iframe.html'
    };
    var loginButtonEls: NodeListOf<Element> = null;

    //Imperatives
    document.addEventListener('DOMContentLoaded', function() {
        setupLoginButtons();
    });
    window.addEventListener('message', function(event) {
        if (event.origin === "https://cdn.colab.duke.edu") {
            //do something
        }
        if (event.data === "netidLoginRequested") {
            login();
        }
    });

    //public functions
    export function init(options: initOptions): void {
        if (typeof options === 'object') {
            for (var i in options) {
                settings[i] = options[i];
            }
        }
        setInterval(checkIfTokenExpired, 30000);
        http.setHeaders({ 'x-api-key': settings.clientId });
    }

    export function login(): Promise<Identity> {
        //set state to random number
        settings.state = Math.round(1 + Math.random() * 4294967296);
        if (localStorage['scopes']) {
            try {
                var oldScopes: string[] = JSON.parse(localStorage['scopes']);
                var canReuseToken: boolean = true;
                for (var i = 0; i < settings.scopes.length; i++) {
                    canReuseToken = canReuseToken && oldScopes.indexOf(settings.scopes[i]) < 0;
                }
                if (canReuseToken) {
                    setToken({ state: "" + settings.state, access_token: localStorage['token'], expires_in: localStorage['tokenExpiration'] });
                    return updateMe();
                }
            } catch (e) {
                console.log("Scopes found in cache, but couldn't parse them");
            }
        }

        var qs = "?response_type=token"
            + "&scope=" + encodeURIComponent(settings.scopes.join(' '))
            + "&client_id=" + encodeURIComponent(settings.clientId)
            + "&state=" + encodeURIComponent("" + settings.state)
            + "&redirect_uri=" + encodeURIComponent(qualifyURL('oauth2callback.html'))
        window.open('https://' + settings.oauthHost + settings.oauthAuthorizationPath + qs, '_blank');
        return new Promise((resolve, reject) => {
            on('tokenset', (token) => {
                resolve({});
            })
            on('tokenerror', (err) => {
                reject(err);
            })
        })
            .then(updateMe);
    }

    export function setToken(tokenResult: { state?: string, access_token?: string, expires_in?: string, scope?: string, token_type?: string, error?: string, error_description?: string }): void {
        if (parseInt(tokenResult.state, 10) !== settings.state) {
            //ignore instead
            console.log('State mismatch: ' + settings.state + ' (correct) vs. ' + parseInt(tokenResult.state, 10) + '. Ignoring...');
            return;
            //dispatch('tokenerror', 'Invalid state. Possible MITM.');
        }
        if (tokenResult.error) {
            dispatch('tokenerror', tokenResult.error + ': ' + tokenResult.error_description);
        }
        settings.token = tokenResult.access_token;
        http.setHeaders({ 'Authorization': 'Bearer ' + settings.token });
        settings.tokenExpiration = Date.now() + 1000 * parseInt(tokenResult.expires_in, 10);
        localStorage['token'] = settings.token;
        localStorage['tokenExpiration'] = settings.tokenExpiration;
        localStorage['scopes'] = JSON.stringify(decodeURIComponent(tokenResult.scope).split(/[\s\+]/));
        dispatch('tokenset', settings.token);
    }

    export function updateMe(): Promise<Identity> {
        return http.getJSON('https://api.colab.duke.edu/identity/v1')
            .then(function(m: Identity): Identity {
                me = m;
                for (var i = 0; i < loginButtonEls.length; i++) {
                    //var loggedInEl = document.createElement('span');
                    console.log("Logged in as " + me['displayName'] + " (" + me['netid'] + ")");
                    //loggedInEl.innerText = "Logged in as " + me['displayName'] + " (" + me['netid'] + ")";
                    //loggedInEl.className = "netid-login logged-in";
                    //loginButtonEls[i].parentElement.replaceChild(loggedInEl, loginButtonEls[i]);
                }
                return me;
            });
    }

    export function getIdentity(netid: string): Promise<Identity> {
        return http.getJSON('https://api.colab.duke.edu/identity/' + netid);
    }

    //private functions
    function setupLoginButtons(): void {
        loginButtonEls = loginButtonEls || document.querySelectorAll(settings.loginButtonSelector);
        for (var i = 0; i < loginButtonEls.length; i++) {
            var loginButton = document.createElement('iframe');
            loginButton.className = 'netid-login';
            loginButton.src = settings.loginButtonSource;
            loginButton.width = '250px';
            loginButton.height = '49px';
            loginButton.frameBorder = '0';


            loginButtonEls[i].parentElement.replaceChild(loginButton, loginButtonEls[i]);

        }
        loginButtonEls = document.querySelectorAll('.netid-login');
        on('tokenset', function() {
            for (var i = 0; i < loginButtonEls.length; i++) {
                loginButtonEls[i].remove();
            }
        });
    }

    function qualifyURL(url: string): string {//Ugly. Thanks bobince of http://stackoverflow.com/questions/470832/getting-an-absolute-url-from-a-relative-one-ie6-issue
        function escapeHTML(s: string): string {
            return s.split('&').join('&amp;').split('<').join('&lt;').split('"').join('&quot;');
        }
        var el = document.createElement('div');
        el.innerHTML = '<a href="' + escapeHTML(url) + '">x</a>';
        return (<HTMLAnchorElement>el.firstChild).href;
    }

    function checkIfTokenExpired(): void {
        if (Date.now() + 30000 > settings.tokenExpiration) {
            onTokenExpired();
        }
    }

    function onTokenExpired(): void {
        var loginAgain = confirm('Your netid session is about to expire, want to log in again?');
        if (settings.token && loginAgain) {
            login();
        } else {
            setupLoginButtons();
            settings.token = null;
        }
    }


    //classes
    /*export class Identity {
        netid: string;
        details: any;

        constructor(new_netid: string) {
            this.netid = new_netid;
        }

        updateDetails(): P.Promise<Identity> {
            return http.getJSON('https://api.colab.duke.edu/identity/v1/' + this.netid)
                .then(((res) => { 
                    for (var i in res) {
                        this.details[i] = res[i]; 
                    }
                    return this;
                }).bind(this));
        }
    }*/

    //interfaces
    export interface Identity { [index: string]: any };
    export interface initOptions {
        clientId: string;
        scopes: string[];
        state: number;
        token: string;
        tokenExpiration: number;
        loginButtonSelector: string;
        oauthHost: string;
        oauthAuthorizationPath: string;
        loginButtonSource: string;
    }

}